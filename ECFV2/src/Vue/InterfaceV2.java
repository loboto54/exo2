package Vue;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import modele.Achat;
import modele.Adresse;
import modele.Client;
import modele.ElementPanier;
import modele.Medecin;
import modele.Medicament;
import modele.Mutuelle;
import modele.Ordonance;
import modele.Specialiste;

public class InterfaceV2 extends JFrame {

	private JPanel tfemail3;
	private JTextField tfnom;
	private JTextField textField_7;
	private JTextField tfprenom;
	private JTextField tfnumrue;
	private JTextField tfrue;
	private JTextField tfville;
	private JTextField tftelephone;
	private JTextField tfemail;
	private JTextField tfnumsecu;
	private JTextField tfdatenaissance;
	private JTextField tfmutuelle;
	private JTextField tfmedecin;
	private JTextField tfemailmutuelle;
	private JTextField tfnommutuelle;
	private JTextField tfruemutuelle;
	private JTextField tfcodepostalmutuelle;
	private JTextField tfvillemutuelle;
	private JTextField tfdepartement;
	private JTextField tftaux;
	private JTextField tfmedocajouter;
	private JTextField tfquantite;
	private JTextField tfmedocretirer;
	private JTextField tfquantite2;
	private JTextField tfNom;
	private JTextField tfNumSecu;
	private JTextField tfnommedoc;
	private JTextField tfprix;
	private JTextField tfDateService;
	private JTextField tfQuantite;
	private JTextField tfcodepostal;
	private JTextField textField;
	private JTextField textField_1;

	/*
	 * Medicament medicament = new Medicament (tfNomMedoc.getText(),
	 * tfcategorie.getText(), tfPrix.getText(), tfQuantite.getText(),
	 * tfDateService.getText());
	 * 
	 * 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceV2 frame = new InterfaceV2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfaceV2() {

		// liste Client
		ArrayList<ElementPanier> panier = new ArrayList();
		ArrayList<Achat> listhistoriqueAchat = new ArrayList();
		ArrayList<Medicament> listmedicaments = new ArrayList();

		ArrayList<Client> listClients = new ArrayList<>();
		Adresse adresse1 = new Adresse(20, "rue de la liberte", 54300, "Chanteheux");
		Mutuelle mutuelle1 = new Mutuelle("GMF", "gmf@gmail.com", adresse1, "Meurthe et Moselle", 40);
		Calendar dateArnaud = Calendar.getInstance();
		dateArnaud.set(1991, 6, 23);
		Client client = new Client("Arnaud", "FRANTZ", 74885, "arnaud@gmail.com", "525124", "GMF", adresse1,
				"01/10/1999");
		System.out.println(client);

		Medicament medicament1 = new Medicament("Serum", "analgésique", 10, 1, 2010);
		Medicament medicament2 = new Medicament("Eferalgan", "analgésique", 10, 1, 2012);
		Medicament medicament3 = new Medicament("Doliprane", "analgésique", 10, 1, 2025);
		Medicament medicament4 = new Medicament("Aspririne", "analgésique", 10, 1, 2012);

		Medecin medecin1 = new Medecin("Brice", "Shwartz", null, 9658, "agegeg@gmail.com", 568);
		Medecin medecin2 = new Medecin("Mathieu", "Kan", null, 7846, "agegeg@gmail.com", 784);

		Specialiste specialiste1 = new Specialiste("Brice", "Ken", 7846, "georgesk@gmail.com", "dentiste");
		Specialiste specialiste2 = new Specialiste("Mathieu", "Ken", 7846, "georgesk@gmail.com", "pediatre");

		Ordonance ordonance1 = new Ordonance(null, "Georges", null, null, null);
		Ordonance ordonance2 = new Ordonance(null, "Georges", null, null, null);

		/*
		 * Ajouter Client
		 * 
		 * listClients.add(new
		 * Client("Arnaud","Frantz",04405,"rezrezr@gmail.com",011010,GMF"));
		 */

		// liste Medicaments
		ArrayList<Medicament> listMedicaments = new ArrayList<>();
		listMedicaments.add(medicament1);
		// Afficher medicaments

		// liste Medecin
		ArrayList<Medecin> listMedecins = new ArrayList<>();
		listMedecins.add(medecin1);
		// Afficher

		// liste Spécialiste
		ArrayList<Specialiste> listSpecialistes = new ArrayList<>();
		// Afficher
		listSpecialistes.add(specialiste1);
		for (Specialiste specialiste : listSpecialistes) {
			System.out.println();
		}
		// liste Adresse

		ArrayList<Adresse> listAdresses = new ArrayList<>();
		// Afficher

		// liste Ordonances

		ArrayList<Ordonance> listOrdonances = new ArrayList<>();
		// Afficher
		for (Ordonance ordonance : listOrdonances) {
			System.out.println();
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(500, 50, 1000, 1000);
		tfemail3 = new JPanel();
		tfemail3.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(tfemail3);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		// Onglets
		JPanel acceuil = new JPanel();
		tabbedPane.addTab("Acceuil", null, acceuil, null);

		JPanel detailClient = new JPanel();
		tabbedPane.addTab("DetailClient", null, detailClient, null);

		JComboBox cbClient = new JComboBox();

		JButton buttonValider = new JButton("valider");

		JLabel lblNewLabel_2 = new JLabel("Selectionnez un client");
		GroupLayout gl_detailClient = new GroupLayout(detailClient);
		gl_detailClient.setHorizontalGroup(gl_detailClient.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_detailClient.createSequentialGroup().addContainerGap().addGroup(gl_detailClient
						.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_detailClient.createSequentialGroup()
								.addComponent(cbClient, GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(buttonValider))
						.addComponent(lblNewLabel_2)).addContainerGap(687, Short.MAX_VALUE)));
		gl_detailClient.setVerticalGroup(gl_detailClient.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_detailClient.createSequentialGroup().addGap(50).addComponent(lblNewLabel_2)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_detailClient.createParallelGroup(Alignment.BASELINE)
								.addComponent(cbClient, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(buttonValider))
						.addContainerGap(154, Short.MAX_VALUE)));
		detailClient.setLayout(gl_detailClient);

		JPanel panel_1 = new JPanel();

		JPanel inscriptionClient = new JPanel();
		tabbedPane.addTab("Inscription Client", null, inscriptionClient, null);

		JPanel effectuerAchat = new JPanel();
		tabbedPane.addTab("EffectuerAchat", null, effectuerAchat, null);

		// TEXTFIEL AJOUTER MEDOC
		tfmedocajouter = new JTextField();
		tfmedocajouter.setBounds(147, 31, 86, 20);
		tfmedocajouter.setColumns(10);

		// TEXTFIEL AJOUTER QUANTITE
		tfquantite = new JTextField();
		tfquantite.setBounds(147, 57, 86, 20);
		tfquantite.setColumns(10);

		// TEXTFIELD RETIRER NOM medoc

		tfmedocretirer = new JTextField();
		tfmedocretirer.setBounds(379, 31, 86, 20);
		tfmedocretirer.setColumns(10);

		// TESXTFIELD RETIRER Quantite

		tfquantite2 = new JTextField();
		tfquantite2.setBounds(379, 57, 86, 20);
		tfquantite2.setColumns(10);

		// AJOUTER MEDOC AU PANIER
		/////////////////////////////
		JButton ButtonSauvegarder = new JButton("Sauvegarder");
		ButtonSauvegarder.setBounds(271, 173, 149, 23);
		ButtonSauvegarder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Adresse adresse5 = new Adresse(Integer.parseInt(tfnumrue.getText()), tfrue.getText(),
						Integer.parseInt(tfcodepostal.getText()), tfville.getText());
				Client client = new Client(tfprenom.getText(), tfNom.getText(), Integer.parseInt(tftelephone.getText()),
						tfemail.getText(), tfnumsecu.getText(), tfmutuelle.getText(), adresse5,
						tfdatenaissance.getText());
				System.out.println(client);

				listClients.add(client);
				for (int i = 0; i < listClients.size(); i++) {
					System.out.println("client n° " + i + " = " + listClients.get(i));
				}

			}
		});
		inscriptionClient.add(ButtonSauvegarder);

		JButton buttonAjouterMedoc = new JButton("Ajouter medoc");
		buttonAjouterMedoc.setBounds(90, 88, 160, 23);
		buttonAjouterMedoc.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.out.println("Bonjour");
			};
			/*
			 * String nomClient = tfNom.getText(); String numSecuClient =
			 * tfNumSecu.getText(); Client clientChoisi = getClientViaSecurite(listClients,
			 * nomClient, numSecuClient); if (clientChoisi != null) {
			 * 
			 * effectueAchat(listhistoriqueAchat, panier, clientChoisi);
			 * System.out.println("Achat effectué : "); System.out.println(listMedicaments);
			 * } else {
			 * 
			 * } System.out.println(listhistoriqueAchat);
			 */
		});

		// Retirer medicament Panier

		JButton buttonEnleverMedoc = new JButton("Enlever medoc");
		buttonEnleverMedoc.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

			}
		});
		buttonEnleverMedoc.setBounds(379, 88, 160, 23);

		JLabel lblNewLabel_1 = new JLabel("ajouter medicament");
		lblNewLabel_1.setBounds(155, 11, 138, 14);

		JLabel lblNewLabel_1_1 = new JLabel("retirer medicament");
		lblNewLabel_1_1.setBounds(379, 14, 123, 14);

		JButton buttonAjouterOrdonance = new JButton("Importer ordonance");
		buttonAjouterOrdonance.setBounds(776, 7, 165, 23);

		JButton buttonConnexion = new JButton("Valider commande");
		buttonConnexion.setBounds(399, 170, 129, 23);

		tfNom = new JTextField();
		tfNom.setBounds(119, 171, 86, 20);
		tfNom.setColumns(10);

		tfNumSecu = new JTextField();
		tfNumSecu.setBounds(264, 171, 86, 20);
		tfNumSecu.setColumns(10);

		JLabel lblNom_1 = new JLabel("nom");
		lblNom_1.setBounds(90, 174, 103, 14);

		JLabel lblNumSecu = new JLabel("N° secu");
		lblNumSecu.setBounds(222, 174, 93, 14);

		JLabel lblNommedocAchat = new JLabel("Nom du medoc");
		lblNommedocAchat.setBounds(23, 34, 114, 14);

		JLabel lblQuantiteAchat = new JLabel("quantite");
		lblQuantiteAchat.setBounds(23, 60, 86, 14);

		JLabel lblNommedocAchat_1 = new JLabel("Nom du medoc");
		lblNommedocAchat_1.setBounds(275, 34, 86, 14);

		JLabel lblQuantiteAchat_1 = new JLabel("quantite");
		lblQuantiteAchat_1.setBounds(275, 60, 86, 14);
		effectuerAchat.setLayout(null);
		effectuerAchat.add(lblNommedocAchat);
		effectuerAchat.add(lblQuantiteAchat);
		effectuerAchat.add(buttonAjouterMedoc);
		effectuerAchat.add(tfquantite);
		effectuerAchat.add(tfmedocajouter);
		effectuerAchat.add(lblNewLabel_1);
		effectuerAchat.add(lblNommedocAchat_1);
		effectuerAchat.add(lblQuantiteAchat_1);
		effectuerAchat.add(lblNewLabel_1_1);
		effectuerAchat.add(tfmedocretirer);
		effectuerAchat.add(tfquantite2);
		effectuerAchat.add(lblNumSecu);
		effectuerAchat.add(lblNom_1);
		effectuerAchat.add(tfNom);
		effectuerAchat.add(tfNumSecu);
		effectuerAchat.add(buttonConnexion);
		effectuerAchat.add(buttonEnleverMedoc);
		effectuerAchat.add(buttonAjouterOrdonance);

		JLabel lblNewLabel_3 = new JLabel(
				"Pour valider la commande , merci d'entrer votre nom et N°secu puis cliquer sur valider commande");
		lblNewLabel_3.setBounds(10, 149, 694, 14);
		effectuerAchat.add(lblNewLabel_3);

		JPanel detailsOrdonance = new JPanel();
		tabbedPane.addTab("DetailsOrdonance", null, detailsOrdonance, null);
		detailsOrdonance.setLayout(null);

		JButton buttonImportOrdonance = new JButton("importer ordonance");
		buttonImportOrdonance.setBounds(37, 55, 232, 23);
		detailsOrdonance.add(buttonImportOrdonance);

		JPanel fenMutuelle = new JPanel();
		tabbedPane.addTab("Ajouter Mutuelle", null, fenMutuelle, null);

		tfnommutuelle = new JTextField();
		tfnommutuelle.setBounds(238, 5, 86, 20);
		tfnommutuelle.setColumns(10);

		tfemailmutuelle = new JTextField();
		tfemailmutuelle.setBounds(238, 36, 86, 20);
		tfemailmutuelle.setColumns(10);

		tfruemutuelle = new JTextField();
		tfruemutuelle.setBounds(238, 67, 86, 20);
		tfruemutuelle.setColumns(10);

		tfcodepostalmutuelle = new JTextField();
		tfcodepostalmutuelle.setBounds(238, 93, 86, 20);
		tfcodepostalmutuelle.setColumns(10);

		tfvillemutuelle = new JTextField();
		tfvillemutuelle.setBounds(238, 124, 86, 20);
		tfvillemutuelle.setColumns(10);

		JLabel lblNom = new JLabel("nom");
		lblNom.setBounds(121, 8, 55, 14);

		JLabel lblemailmutuelle = new JLabel("email");
		lblemailmutuelle.setBounds(121, 39, 83, 14);

		JLabel lblRue_1 = new JLabel("rue");
		lblRue_1.setBounds(121, 70, 55, 14);

		JLabel lblCodePostal = new JLabel("code postal");
		lblCodePostal.setBounds(121, 96, 83, 14);

		JLabel lblVille_2 = new JLabel("ville");
		lblVille_2.setBounds(121, 127, 83, 14);

		JLabel lblVille_2_1 = new JLabel("departement");
		lblVille_2_1.setBounds(71, 158, 149, 14);

		tfdepartement = new JTextField();
		tfdepartement.setBounds(238, 155, 86, 20);
		tfdepartement.setColumns(10);

		tftaux = new JTextField();
		tftaux.setBounds(238, 181, 86, 20);
		tftaux.setColumns(10);

		JLabel lblNewLabel = new JLabel("Rembourssement");
		lblNewLabel.setBounds(71, 184, 133, 14);
		fenMutuelle.setLayout(null);
		fenMutuelle.add(lblCodePostal);
		fenMutuelle.add(lblVille_2_1);
		fenMutuelle.add(lblNom);
		fenMutuelle.add(lblemailmutuelle);
		fenMutuelle.add(lblNewLabel);
		fenMutuelle.add(lblVille_2);
		fenMutuelle.add(lblRue_1);
		fenMutuelle.add(tftaux);
		fenMutuelle.add(tfvillemutuelle);
		fenMutuelle.add(tfcodepostalmutuelle);
		fenMutuelle.add(tfruemutuelle);
		fenMutuelle.add(tfemailmutuelle);
		fenMutuelle.add(tfnommutuelle);
		fenMutuelle.add(tfdepartement);
		acceuil.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(611, 11, 10, 10);
		acceuil.add(panel);
		inscriptionClient.setLayout(null);

		JLabel lblVille = new JLabel("prenom");
		lblVille.setBounds(5, 8, 67, 14);
		inscriptionClient.add(lblVille);

		tfprenom = new JTextField();
		tfprenom.setBounds(82, 5, 86, 20);
		inscriptionClient.add(tfprenom);
		tfprenom.setColumns(10);

		// Recupérer textfield
		String prenom = tfprenom.getText();

		JLabel lblNomRue = new JLabel("nom");
		lblNomRue.setBounds(5, 32, 54, 14);
		inscriptionClient.add(lblNomRue);

		tfnom = new JTextField();
		tfnom.setBounds(82, 29, 86, 20);
		inscriptionClient.add(tfnom);
		tfnom.setColumns(10);

		// Recupérer textfield
		String nom = tfnom.getText();

		tfnumrue = new JTextField();
		tfnumrue.setBounds(82, 57, 86, 20);
		tfnumrue.setColumns(10);
		inscriptionClient.add(tfnumrue);

		// Recupérer textfield
		String nomrue = tfnumrue.getText();

		tfrue = new JTextField();
		tfrue.setBounds(82, 85, 86, 20);
		tfrue.setColumns(10);
		inscriptionClient.add(tfrue);

		// Recupérer textfield
		String rue = tfrue.getText();

		tfville = new JTextField();
		tfville.setBounds(82, 112, 86, 20);
		tfville.setColumns(10);
		inscriptionClient.add(tfville);

		// Recupérer textfield
		String ville = tfville.getText();

		tftelephone = new JTextField();
		tftelephone.setBounds(354, 85, 86, 20);
		tftelephone.setColumns(10);
		inscriptionClient.add(tftelephone);

		// Recupérer textfield
		String telephone = tftelephone.getText();

		tfemail = new JTextField();
		tfemail.setBounds(82, 165, 86, 20);
		tfemail.setColumns(10);
		inscriptionClient.add(tfemail);

		// Recupérer textfield
		String email = tfemail.getText();

		tfnumsecu = new JTextField();
		tfnumsecu.setBounds(82, 190, 86, 20);
		tfnumsecu.setColumns(10);
		inscriptionClient.add(tfnumsecu);

		// Recupérer textfield
		String secu = tfnumsecu.getText();

		JLabel lblNumeroRue = new JLabel("numero rue");
		lblNumeroRue.setBounds(5, 60, 67, 14);
		inscriptionClient.add(lblNumeroRue);

		JLabel lblRue = new JLabel("rue");
		lblRue.setBounds(5, 88, 54, 14);
		inscriptionClient.add(lblRue);

		JLabel lblVille_1 = new JLabel("ville");
		lblVille_1.setBounds(5, 115, 54, 14);
		inscriptionClient.add(lblVille_1);

		JLabel lblTelephone = new JLabel("telephone");
		lblTelephone.setBounds(242, 88, 91, 14);
		inscriptionClient.add(lblTelephone);

		JLabel lblEmail = new JLabel("email");
		lblEmail.setBounds(5, 168, 54, 14);
		inscriptionClient.add(lblEmail);

		JLabel lblNSecu = new JLabel("n° secu");
		lblNSecu.setBounds(5, 193, 54, 14);
		inscriptionClient.add(lblNSecu);

		tfdatenaissance = new JTextField();
		tfdatenaissance.setBounds(354, 5, 86, 20);
		tfdatenaissance.setColumns(10);

		inscriptionClient.add(tfdatenaissance);
		// Recupérer textfield

		tfmutuelle = new JTextField();
		tfmutuelle.setBounds(354, 29, 86, 20);
		tfmutuelle.setColumns(10);
		inscriptionClient.add(tfmutuelle);
		// Recupérer textfield
		String mutuelle = tfmutuelle.getText();

		tfmedecin = new JTextField();
		tfmedecin.setBounds(354, 57, 86, 20);
		tfmedecin.setColumns(10);
		inscriptionClient.add(tfmedecin);
		// Recupérer textfield
		String medecin = tfmedecin.getText();

		JLabel lblDateDeNaissance = new JLabel("date de naissance");
		lblDateDeNaissance.setBounds(223, 8, 121, 14);
		inscriptionClient.add(lblDateDeNaissance);

		JLabel lblMutuelle = new JLabel("mutuelle");
		lblMutuelle.setBounds(242, 32, 82, 14);
		inscriptionClient.add(lblMutuelle);

		JLabel lblMedecinTraitant = new JLabel("medecin traitant");
		lblMedecinTraitant.setBounds(223, 60, 101, 14);
		inscriptionClient.add(lblMedecinTraitant);

		// AJOUTER BOUTON SAUVEGARDER CLIENT

		JLabel lblcodepostal = new JLabel("code postal");
		lblcodepostal.setBounds(5, 143, 79, 14);
		inscriptionClient.add(lblcodepostal);

		tfcodepostal = new JTextField();
		tfcodepostal.setBounds(82, 140, 86, 20);
		inscriptionClient.add(tfcodepostal);
		tfcodepostal.setColumns(10);

		JLabel lblFormatDate = new JLabel("La date doit être au format DD/MM/AAAA");
		lblFormatDate.setBounds(498, 8, 261, 14);
		inscriptionClient.add(lblFormatDate);

		JButton buttonRetour = new JButton("Retour");
		buttonRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		JButton buttonquitter = new JButton("quitter");
		GroupLayout gl_tfemail3 = new GroupLayout(tfemail3);
		gl_tfemail3.setHorizontalGroup(gl_tfemail3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_tfemail3.createSequentialGroup()
						.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addContainerGap(18, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_tfemail3.createSequentialGroup().addContainerGap(697, Short.MAX_VALUE)
						.addComponent(buttonRetour).addGap(18).addComponent(buttonquitter).addGap(81)));
		gl_tfemail3.setVerticalGroup(gl_tfemail3.createParallelGroup(Alignment.LEADING).addGroup(gl_tfemail3
				.createSequentialGroup().addGap(5)
				.addComponent(tabbedPane, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(ComponentPlacement.RELATED).addGroup(gl_tfemail3
						.createParallelGroup(Alignment.BASELINE).addComponent(buttonRetour).addComponent(buttonquitter))
				.addContainerGap(637, Short.MAX_VALUE)));

		JPanel HistoriqueAchat = new JPanel();
		tabbedPane.addTab("Historique d'achat", null, HistoriqueAchat, null);

		JLabel lblNom_1_1 = new JLabel("nom");
		lblNom_1_1.setBounds(707, 14, 58, 14);

		textField = new JTextField();
		textField.setBounds(770, 11, 86, 20);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(770, 37, 86, 20);
		textField_1.setColumns(10);

		JLabel lblNumSecu_1 = new JLabel("N° secu");
		lblNumSecu_1.setBounds(693, 40, 73, 14);
		HistoriqueAchat.setLayout(null);

		JButton buttonConnexion_1 = new JButton("Connexion");
		buttonConnexion_1.setBounds(773, 63, 83, 23);
		HistoriqueAchat.add(buttonConnexion_1);
		HistoriqueAchat.add(lblNom_1_1);
		HistoriqueAchat.add(lblNumSecu_1);
		HistoriqueAchat.add(textField_1);
		HistoriqueAchat.add(textField);

		JPanel ajouter_medoc = new JPanel();
		tabbedPane.addTab("Ajouter medicament", null, ajouter_medoc, null);

		JLabel lblNomMedoc = new JLabel("nom");
		lblNomMedoc.setBounds(38, 32, 59, 14);

		tfnommedoc = new JTextField();
		tfnommedoc.setBounds(132, 29, 86, 20);
		tfnommedoc.setColumns(10);

		JLabel lblCategorie = new JLabel("categorie");
		lblCategorie.setBounds(38, 71, 86, 14);

		JLabel lblPrix = new JLabel("Prix");
		lblPrix.setBounds(38, 110, 59, 14);

		JLabel lblDateService = new JLabel("Date de service");
		lblDateService.setBounds(38, 148, 86, 14);

		JLabel lblQuantite = new JLabel("quantité");
		lblQuantite.setBounds(38, 186, 59, 14);

		tfprix = new JTextField();
		tfprix.setBounds(131, 107, 86, 20);
		tfprix.setColumns(10);
		// Recupérer textfield
		String prix = tfprix.getText();

		tfDateService = new JTextField();
		tfDateService.setBounds(131, 145, 86, 20);
		tfDateService.setColumns(10);
		// Recupérer textfield
		String DateService = tfDateService.getText();

		tfQuantite = new JTextField();
		tfQuantite.setBounds(131, 183, 86, 20);
		tfQuantite.setColumns(10);
		// Recupérer textfield
		String quantite = tfQuantite.getText();

		JComboBox CBcategorie = new JComboBox();
		CBcategorie.setBounds(132, 67, 101, 22);

		JButton btnNewButton_4 = new JButton("Enregistrer");
		btnNewButton_4.setBounds(180, 214, 85, 23);
		ajouter_medoc.setLayout(null);
		ajouter_medoc.add(lblNomMedoc);
		ajouter_medoc.add(tfnommedoc);
		ajouter_medoc.add(btnNewButton_4);
		ajouter_medoc.add(lblCategorie);
		ajouter_medoc.add(CBcategorie);
		ajouter_medoc.add(lblPrix);
		ajouter_medoc.add(tfprix);
		ajouter_medoc.add(lblQuantite);
		ajouter_medoc.add(tfQuantite);
		ajouter_medoc.add(lblDateService);
		ajouter_medoc.add(tfDateService);
		tfemail3.setLayout(gl_tfemail3);

	}

	private static boolean ajoutePanier(ArrayList<ElementPanier> panier, Medicament medicament, int quantite) {
		// On teste si le stock est suffisant
		if (medicament.getQuantite() >= quantite) {
			ElementPanier element = new ElementPanier(medicament, quantite);
			panier.add(element);
			return true;
		} else {
			return false;
		}
	}

	private static Client getClientViaSecurite(ArrayList<Client> listeClient, String nom, String numSecu) {
		int tailleListe = listeClient.size();
		for (int i = 0; i < tailleListe; i++) {
			Client client = listeClient.get(i);
			if (client.is(nom, numSecu)) {
				return client;
			}
		}
		return null;
	}

	private static void effectueAchat(ArrayList<Achat> historiqueAchat, ArrayList<ElementPanier> panier,
			Client client) {
		panier.forEach((element) -> {
			Medicament medicament = element.getMedicament();
			medicament.setQuantite(element.getQuantite());
		});
		Achat achat = new Achat(panier, client);
		historiqueAchat.add(achat);
		panier = new ArrayList(); // on fait ça pour vider le panier
	}
}