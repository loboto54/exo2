package modele;

public class ElementPanier {
	Medicament medicament;
	int quantite;
	double totalElement;

	public ElementPanier(Medicament medicament, int quantite) {
		this.medicament = medicament;
		this.quantite = quantite;
		this.totalElement = quantite * medicament.getPrix();
	}

	public Medicament getMedicament() {
		return medicament;
	}

	public void setMedicament(Medicament medicament) {
		this.medicament = medicament;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public double getTotalElement() {
		return totalElement;
	}

	public void setTotalElement(double totalElement) {
		this.totalElement = totalElement;
	}

	@Override
	public String toString() {
		return quantite + "x " + medicament.getNomMedicament() + " (" + totalElement + "€)";
	}

}
