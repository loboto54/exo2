package modele;

public class Medicament {
	String nomMedicament;
	String categorie;
	int dateService;
	double prix;
	int quantite;

	public Medicament(String nomMedicament, String categorie, double prix, int quantite, int i) {
		this.setNomMedicament(nomMedicament);
		this.setCategorie(categorie);
		this.setPrix(prix);
		this.setQuantite(quantite);
		this.setDateService(i);
	}

	public int getDateService(int dateService) {
		return dateService;
	}

	public void setDateService(int dateService) {
		this.dateService = dateService;
	}

	public String getNomMedicament() {
		return nomMedicament;
	}

	public void setNomMedicament(String nomMedicament) {
		this.nomMedicament = nomMedicament;
	}

	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

}
