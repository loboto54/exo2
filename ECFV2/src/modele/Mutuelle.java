package modele;

public class Mutuelle {
	String nomMutuelle;
	String emailMutuelle;
	Adresse adresse;
	String departement;
	int rembourssementMutuelle;

	public Mutuelle(String nomMutuelle, String emailMutuelle, Adresse adresse, String departement,
			int rembourssementMutuelle) {
		this.setNomMutuelle(nomMutuelle);
		this.setEmailMutuelle(emailMutuelle);
		this.setAdresse(adresse);
		this.setDepartement(departement);
		this.setRembourssementMutuelle(rembourssementMutuelle);
	}

	public String getNomMutuelle() {
		return nomMutuelle;
	}

	public void setNomMutuelle(String nomMutuelle) {
		this.nomMutuelle = nomMutuelle;
	}

	public String getEmailMutuelle() {
		return emailMutuelle;
	}

	public void setEmailMutuelle(String emailMutuelle) {
		this.emailMutuelle = emailMutuelle;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public int getRembourssementMutuelle() {
		return rembourssementMutuelle;
	}

	public void setRembourssementMutuelle(int rembourssementMutuelle) {
		this.rembourssementMutuelle = rembourssementMutuelle;
	}
}
