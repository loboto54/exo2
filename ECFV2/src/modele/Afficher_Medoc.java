package modele;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Afficher_Medoc extends JPanel {
	// Header de JTable
	String[] columns = new String[] { "Nom Medoc", "Categorie", "Prix", "Ordonance", "Quantite", "Date Service",

	};
	// données pour JTable dans un tableau 2D
	Object[][] data = new Object[][] { { 1, "1", "2", 3, 4, 5 }, { 1, "1", "2", 3, 4, 5 }, { 1, "1", "2", 3, 4, 5 },
			{ 1, "1", "2", 3, 4, 5 }, };
	// crée un JTable avec des données
	JTable table = new JTable(data, columns);
	TableRowSorter<TableModel> sort = new TableRowSorter<>(table.getModel());
	JTextField textField = new JTextField();

	public Afficher_Medoc() {
		// définir la largeur de la 3éme colonne sur 200 pixels
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(2).setPreferredWidth(200);

		table.setRowSorter(sort);
		JPanel p = new JPanel(new BorderLayout());
		p.add(new JLabel("Chercher:"), BorderLayout.WEST);
		p.add(textField, BorderLayout.CENTER);
		setLayout(new BorderLayout());
		add(p, BorderLayout.SOUTH);
		add(new JScrollPane(table), BorderLayout.CENTER);
		textField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				String str = textField.getText();
				if (str.trim().length() == 0) {
					sort.setRowFilter(null);
				} else {
					// (?i) recherche insensible à la casse
					sort.setRowFilter(RowFilter.regexFilter("(?i)" + str));
				}
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				String str = textField.getText();
				if (str.trim().length() == 0) {
					sort.setRowFilter(null);
				} else {
					sort.setRowFilter(RowFilter.regexFilter("(?i)" + str));
				}
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame f = new JFrame("Chercher dans JTable");
				f.add(new Afficher_Medoc());
				f.setSize(1000, 280);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setLocationRelativeTo(null);
				f.setVisible(true);
			}
		});
	}

}