package modele;

public class Adresse {

	private int numero;
	private String nomrue;
	private int codepostal;
	private String nomville;

	public Adresse(int numero, String nomrue, int codepostal, String nomville) {

		this.numero = numero;
		this.nomrue = nomrue;
		this.codepostal = codepostal;
		this.nomville = nomville;
	}

	/**
	 * @return
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * @param numero
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * @return
	 */
	public String getNomrue() {
		return nomrue;
	}

	/**
	 * @param nomrue
	 */
	public void setNomrue(String nomrue) {
		this.nomrue = nomrue;
	}

	/**
	 * @return
	 */
	public int getCodepostal() {
		return codepostal;
	}

	/**
	 * @param codepostal
	 */
	public void setCodepostal(int codepostal) {
		this.codepostal = codepostal;
	}

	/**
	 * @return
	 */
	public String getNomville() {
		return nomville;
	}

	/**
	 * @param nomville
	 */
	public void setNomville(String nomville) {
		this.nomville = nomville;
	}

	public void setAdresse(int numero, String nomrue, int codepostal, String nomville) {

		this.numero = numero;
		this.nomrue = nomrue;
		this.codepostal = codepostal;
		this.nomville = nomville;
	}

	public void afficher() {
		System.out.println("l'adresse est " + numero + nomrue + codepostal + nomville);
	}

	/**
	 * @param nom
	 * @param prenom
	 * @param age
	 * @param adresse
	 */
	public void afficher2(String nom, String prenom, String age, Adresse adresse) {
		System.out.println("l'adresse de " + nom + prenom + age + "est" + adresse);
	}

	/**
	 *
	 */
	public String toString() {
		return (numero + nomrue + codepostal + nomville);
	}

}
