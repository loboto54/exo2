package modele;
import java.awt.BorderLayout;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
public class Afficher_Client extends JPanel 
{
	//Header de JTable 
	String[] columns = new String[] {
			"prenom",
			"nom", 
			"Adresse", 
			"Telephone", 
			"email",
			"num securité",
			"mutuelle",
			"date naissance",
	};
	//données pour JTable dans un tableau 2D
	Object[][] data = new Object[][] {
		{1, "1", "2", 3, 4 ,5,1,1},
		{1, "1", "2", 3, 4 ,5,1,1},
		{1, "1", "2", 3, 4 ,5,1,1},
		{1, "1", "2", 3, 4 ,5,1,1},
	};
	//crée un JTable avec des données
	JTable table = new JTable(data, columns);
	TableRowSorter<TableModel> sort = new TableRowSorter<>(table.getModel());
	JTextField textField = new JTextField();
	public Afficher_Client() 
	{
		//définir la largeur de la 3éme colonne sur 200 pixels
		TableColumnModel columnModel = table.getColumnModel();
		columnModel.getColumn(2).setPreferredWidth(200);

		table.setRowSorter(sort);
		JPanel p = new JPanel(new BorderLayout());
		p.add(new JLabel("Chercher:"), BorderLayout.WEST);
		p.add(textField, BorderLayout.CENTER);
		setLayout(new BorderLayout());
		add(p, BorderLayout.SOUTH);
		add(new JScrollPane(table), BorderLayout.CENTER);
		textField.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent e) {
				String str = textField.getText();
				if (str.trim().length() == 0) {
					sort.setRowFilter(null);
				} else {
					//(?i) recherche insensible à la casse
					sort.setRowFilter(RowFilter.regexFilter("(?i)" + str));
				}
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				String str = textField.getText();
				if (str.trim().length() == 0) {
					sort.setRowFilter(null);
				} else {
					sort.setRowFilter(RowFilter.regexFilter("(?i)" + str));
				}
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
		});
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				JFrame f = new JFrame("Chercher dans JTable");
				f.add(new Afficher_Client());
				f.setSize(1000, 280);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setLocationRelativeTo(null);
				f.setVisible(true);
			}
		});
	}
	//Afficher Client V2
	
	
	
	/*
	 String header [] = {"Nom", "Prenom", "Date de naissance", "Telephone", "Email", "N°Securité Sociale", "Medecin traitant"};
	
    JTable table = new JTable();

    DefaultTableModel model= new DefaultTableModel(header, 0);
    table.setModel(model);
    Object rowData[] = new Object[7];

    for (Client client: LesListes.LesClients) {
        rowData[0] = client.getNom();
        rowData[1] = client.getPrenom();
        rowData[2] = client.getDateDeNaissance();
        rowData[3] = client.getPhone();
        rowData[4] = client.getMail();
        rowData[5] = client.getNumSecuriteSociale();
        rowData[6] = client.getMedecinTraitant();
        model.addRow(rowData);
    }
	 */
	
	
	// Inscrire un client 
	
	/*
	for (Client client : LesListes.getClient())    {

        String numSecu = tfNumSecuDejaClientPC.getText();
        String prenom = tfPrenomDejaClientPC.getText();
        String nom = tfNomDejaClientPC.getText();

        if (numSecu.equals(client.getNumSecuriteSociale()) || prenom.equals(client.getPrenom()) && nom.toUpperCase().equals(client.getNom())){
            new FenetreInfoClient();
            frame.dispose();
        }
        else if(numSecu.equals("") && prenom.equals("") && nom.toUpperCase().equals("")){
            JOptionPane.showMessageDialog(null, "Pour trouver un client, il faut:\nSoit le nom et prénom du client          \nSoit le N° Sécurité Sociale du client     \n ", "Attention!!!", JOptionPane.WARNING_MESSAGE);
        }
        else {
            JOptionPane.showMessageDialog(null, "Saisie Invalide\nVeuillez réessayer!!!    \n ", "Attention!!!", JOptionPane.WARNING_MESSAGE);
        }
        break;
    }*/
	
}