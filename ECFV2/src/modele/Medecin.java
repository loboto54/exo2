package modele;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Medecin extends Personne {
	int agreement;


	public Medecin (String nom , 	String prenom , Adresse adresse , int telephone , String email, int agreement) {
		super(prenom, nom , telephone , email );
		this.adresse=adresse ; 
		this.agreement= agreement ; 
	}
	
	
	    public static void main(String[] args) {	
	 List<String> medecin = Arrays.asList("Charles", "Henri", "Paul");
     
     // Initialise une liste de Medecin
     ArrayList<String> listeMedecin = new ArrayList<>(Arrays.asList("Charles", "Henri", "Paul"));
     
     System.out.println(medecin);
     System.out.println(listeMedecin);
 
}
	public int getRegistration() {
		return agreement;
	}

	public void setRegistration(int registration) {
		this.agreement = agreement;
	}
}