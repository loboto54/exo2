package modele;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Achat {
	ArrayList<ElementPanier> panier;
	Client client;
	GregorianCalendar dateAchat;
	double totalCoutPanier;

	public Achat(ArrayList<ElementPanier> panier, Client client) {
		this.panier = panier;
		this.client = client;
		this.dateAchat = new GregorianCalendar();
		panier.forEach((elementPanier) -> {
			this.totalCoutPanier += elementPanier.getTotalElement();
		});
	}

	@Override
	public String toString() {
		return client + " a acheté des médicaments pour un total de " + totalCoutPanier + "€";
	}

}
