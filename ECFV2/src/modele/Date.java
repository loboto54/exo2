/*
 * package modele;
 * 
 * import java.text.SimpleDateFormat; import java.time.Instant; import
 * java.time.ZoneId; import java.time.ZonedDateTime; import
 * java.util.GregorianCalendar; import java.util.Locale;
 * 
 * public class Date { Instant date; GregorianCalendar dateDeNaissance = new
 * GregorianCalendar(); Calendar dateNaissanceJean = Calendar.getInstance();
 * dateNaissanceJean.set(2005, 6, 8);
 * 
 * protected GregorianCalendar getDate() { ZonedDateTime zonedDateTime =
 * ZonedDateTime.ofInstant(date, ZoneId.systemDefault()); GregorianCalendar
 * calendar = GregorianCalendar.from(zonedDateTime); return calendar; }
 * 
 * protected String datetoString() { String dateFormat = "dd-MMM-yyyy";
 * SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
 * return sdf.format(this.getDate()); }
 * 
 * protected void setDate() { this.date = Instant.now();
 * 
 * }
 * 
 * public long age() { GregorianCalendar today = (GregorianCalendar)
 * GregorianCalendar.getInstance(); int curYear =
 * today.get(GregorianCalendar.YEAR); long age = curYear -
 * patient.dateDeNaissance.get(GregorianCalendar.YEAR); return age; }
 * 
 * protected void setDateDeNaissance(int year, int month, int day) {
 * this.dateDeNaissance.set(year, month - 1, day);
 * 
 * }
 * 
 * }
 */