package modele;

public abstract class Personne {

	String prenom;
	String nom;
	int telephone;
	String email;
	Adresse adresse;

	public Personne(String prenom, String nom, int telephone, String email) {

		this.setPrenom(prenom);
		this.setNom(nom);
		this.setTelephone(telephone);
		this.setEmail(email);

	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom(String nom) {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
