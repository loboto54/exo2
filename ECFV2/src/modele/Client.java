package modele;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Client extends Personne {

	String numsecu;

	String mutuelle;

	LocalDate datenaissance;

	public Client(String prenom, String nom, int telephone, String email, String numsecu, String mutuelle,
			Adresse adresse, String datedenaissanceString) {
		super(prenom, nom, telephone, email);
		this.setNumsecu(numsecu);
		this.setMutuelle(mutuelle);
		this.adresse = adresse;
		this.setDateNaissance(datedenaissanceString);
	}
	public String toString(){
	    return prenom+" "+nom+" "+telephone+" "+email+" "+numsecu+" "+mutuelle+" "+adresse+" "+datenaissance;
	}
	public String getNumsecu() {
		return numsecu;
	}

	public void setNumsecu(String numsecu) {
		this.numsecu = numsecu;
	}

	public String getMutuelle() {
		return mutuelle;
	}

	public void setMutuelle(String mutuelle) {
		this.mutuelle = mutuelle;
	}

	public LocalDate getDataNaissance() {
		return datenaissance;
	}

	public void setDateNaissance(String datenaissance) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate datedenaissancek = LocalDate.parse(datenaissance, formatter);
		this.datenaissance = datedenaissancek;

	}

	public boolean is(String nom, String numsecu) {
		return (this.nom == nom && this.numsecu == numsecu);
	}

}
