package modele;

import java.util.Date;

public class Ordonance {
	Date dateOrdonance;
	String nomMedecin;
	Client client;
	Medicament medicament;
	Specialiste specialiste;

	String email;
	Adresse adresse;
	String departement;
	int taux;

	public Ordonance(Date dateOrdonance, String nomMedecin, Client client, Medicament medicament,
			Specialiste specialiste) {
		this.dateOrdonance = dateOrdonance;
		this.nomMedecin = nomMedecin;
		this.client = client;
		this.medicament = medicament;
		this.specialiste = specialiste;

	}

}
